use GTA
GO

insert into Animal([Name])
values ('Elephant'),
	   ('Lion'),
	   ('Cat'),
	   ('Dog'),
	   ('Duck')
GO

declare @animalId int

-- Elephant
select @animalId = AnimalId from Animal where [Name] = 'Elephant'
insert into Question(AnimalId, Question)
values (@animalId, 'Does it have a trunk?'),
	   (@animalId, 'Is it grey?'),
	   (@animalId, 'Does it have four legs?')

-- Lion
select @animalId = AnimalId from Animal where [Name] = 'Lion'
insert into Question(AnimalId, Question)
values (@animalId, 'Does it have a mane?'),
	   (@animalId, 'Is it yellow?'),
	   (@animalId, 'Does it have four legs?')

-- Cat
select @animalId = AnimalId from Animal where [Name] = 'Cat'
insert into Question(AnimalId, Question)
values (@animalId, 'Does it have claws?'),
	   (@animalId, 'Does it climb trees?'),
	   (@animalId, 'Does it have four legs?')

-- Dog
select @animalId = AnimalId from Animal where [Name] = 'Dog'
insert into Question(AnimalId, Question)
values (@animalId, 'Does it bark?'),
	   (@animalId, 'Does it sometime have spots?'),
	   (@animalId, 'Does it have four legs?')

-- Duck
select @animalId = AnimalId from Animal where [Name] = 'Duck'
insert into Question(AnimalId, Question)
values (@animalId, 'Does it fly?'),
	   (@animalId, 'Does it swim?'),
	   (@animalId, 'Does it have two legs?')

