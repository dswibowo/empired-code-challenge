﻿using System;
using System.Collections.Generic;
using System.Linq;
using GTA.Data;

namespace GTA.Business
{
    /// <summary>
    /// The manager class.
    /// In this project, this is the class which handles all
    /// business logic during the game.
    /// </summary>
    public class Manager
    {
        /// <summary>
        /// List of QuestionId that have been asked to the user.
        /// This include those which have Yes and/or No answers.
        /// </summary>
        private readonly List<int> _previouslyAskedQuestionIds;

        /// <summary>
        /// List of QuestionId that have been asked to the user
        /// where the answer is Yes.
        /// </summary>
        private readonly List<int> _correctQuestionIds;

        /// <summary>
        /// List of animals as available in the database.
        /// In this case must be provided by the constructor.
        /// </summary>
        public List<Animal> Animals { get; }

        /// <summary>
        /// List of questions as available in the database.
        /// In this case must be provided by the constructor.
        /// </summary>
        public List<Question> Questions { get; }

        /// <summary>
        /// Selected animal by the user.
        /// </summary>
        public Animal SelectedAnimal { get; set; }

        /// <summary>
        /// Returns a list of animal ids which may be 
        /// the potential answer to the game.
        /// This is based on questions that have been asked so far.
        /// </summary>
        public List<int> PotentialAnimalIds
        {
            get
            {
                if (!_correctQuestionIds.Any())
                {
                    // Haven't got any correct questions so far.
                    // All animals are potential answers.
                    // NOTE: We can safely say this because we know that the same
                    //       questions will not be asked to the user again.
                    //       See GetNextQuestion() method.
                    //       Otherwise this will potentially lead to infinite loop.
                    return Animals.Select(a => a.AnimalId).ToList();
                }

                // Get the list of actual questions that have been asked to the users
                // where the answer is Yes.
                // We need the actual question string, since some questions can be duplicated 
                // for multiple animals.
                List<string> correctQuestions =
                Questions
                    .Where(q => _correctQuestionIds.Contains(q.QuestionId))
                    .Select(q => q.Question1)
                    .ToList();

                // Get all Animal Ids which have the same question as above.
                return
                    Questions
                        .GroupBy(q => q.AnimalId)
                        .Select(gq => new
                        {
                            AnimalId = gq.Key,
                            Questions = gq.ToList()
                        })
                        .Where(gq => correctQuestions.All(correct => gq.Questions.Any(gqq => gqq.Question1 == correct)))
                        .Select(gq => gq.AnimalId)
                        .ToList();
            }
        }

        /// <summary>
        /// Constructor for this class, initiate animals and their questions.
        /// </summary>
        public Manager()
        {
            _previouslyAskedQuestionIds = new List<int>();
            _correctQuestionIds = new List<int>();

            using (var dc = new GTADBDataContext())
            {
                // Populate the list of animals.
                Animals = dc.Animals.ToList();

                // Populate list of questions.
                Questions = dc.Questions.ToList();
            }
        }

        /// <summary>
        /// Reset member variables for this class.
        /// </summary>
        public void Reset()
        {
            _previouslyAskedQuestionIds.Clear();
            _correctQuestionIds.Clear();
        }

        /// <summary>
        /// Get the next question to be asked to the user.
        /// </summary>
        /// <returns></returns>
        public Question GetNextQuestions()
        {
            List<Question> potentialQuestions;
            if (!_previouslyAskedQuestionIds.Any())
            {
                // No questions have been asked yet, so
                // all questions are potential questions.
                potentialQuestions = Questions;
            }
            else
            {
                // Get a list of previously ask question strings.
                List<string> prevQuestions =
                    Questions.Where(q => _previouslyAskedQuestionIds.Contains(q.QuestionId))
                        .Select(q => q.Question1)
                        .ToList();

                // Get a list of questions applicable to potential animals
                // which have not been asked yet.
                potentialQuestions =
                    Questions
                        .Where(q => PotentialAnimalIds.Contains(q.AnimalId))
                        .Where(q => !prevQuestions.Contains(q.Question1))
                        .Select(q => q)
                        .ToList();
            }

            if (!potentialQuestions.Any())
            {
                throw new Exception("There is no more questions to be asked.");
            }

            Random rd = new Random(DateTime.UtcNow.Millisecond);
            int nextIndex = rd.Next(potentialQuestions.Count - 1);
            Question nextQuestion = potentialQuestions[nextIndex];

            // Add this Question to the _previouslyAskedQuestionIds
            _previouslyAskedQuestionIds.Add(nextQuestion.QuestionId);

            // Return the question.
            return nextQuestion;
        }

        /// <summary>
        /// Submit the question Id where the user has answered Yes to.
        /// </summary>
        public void SubmitCorrectQuestion(int questionId)
        {
            if (_correctQuestionIds.Contains(questionId))
            {
                return;
            }

            _correctQuestionIds.Add(questionId);
        }
    }
}
