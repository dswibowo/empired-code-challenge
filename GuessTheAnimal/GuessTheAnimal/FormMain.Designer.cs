﻿namespace GuessTheAnimal
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabMainControl = new System.Windows.Forms.TabControl();
            this.tabPlay = new System.Windows.Forms.TabPage();
            this.buttonGo = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxAnimal = new System.Windows.Forms.ComboBox();
            this.bsManager = new System.Windows.Forms.BindingSource(this.components);
            this.bsAnimal = new System.Windows.Forms.BindingSource(this.components);
            this.tabAdmin = new System.Windows.Forms.TabPage();
            this.buttonQuit = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tabMainControl.SuspendLayout();
            this.tabPlay.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bsManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsAnimal)).BeginInit();
            this.tabAdmin.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabMainControl
            // 
            this.tabMainControl.Controls.Add(this.tabPlay);
            this.tabMainControl.Controls.Add(this.tabAdmin);
            this.tabMainControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabMainControl.Location = new System.Drawing.Point(0, 0);
            this.tabMainControl.Name = "tabMainControl";
            this.tabMainControl.SelectedIndex = 0;
            this.tabMainControl.Size = new System.Drawing.Size(1178, 519);
            this.tabMainControl.TabIndex = 0;
            // 
            // tabPlay
            // 
            this.tabPlay.Controls.Add(this.buttonQuit);
            this.tabPlay.Controls.Add(this.buttonGo);
            this.tabPlay.Controls.Add(this.label2);
            this.tabPlay.Controls.Add(this.comboBoxAnimal);
            this.tabPlay.Location = new System.Drawing.Point(4, 29);
            this.tabPlay.Name = "tabPlay";
            this.tabPlay.Padding = new System.Windows.Forms.Padding(3);
            this.tabPlay.Size = new System.Drawing.Size(1170, 486);
            this.tabPlay.TabIndex = 0;
            this.tabPlay.Text = "Play";
            this.tabPlay.UseVisualStyleBackColor = true;
            // 
            // buttonGo
            // 
            this.buttonGo.Font = new System.Drawing.Font("Berlin Sans FB", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonGo.Location = new System.Drawing.Point(535, 197);
            this.buttonGo.Name = "buttonGo";
            this.buttonGo.Size = new System.Drawing.Size(75, 38);
            this.buttonGo.TabIndex = 3;
            this.buttonGo.Text = "Go";
            this.buttonGo.UseVisualStyleBackColor = true;
            this.buttonGo.Click += new System.EventHandler(this.ButtonGoClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(220, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(162, 26);
            this.label2.TabIndex = 2;
            this.label2.Text = "Pick an animal";
            // 
            // comboBoxAnimal
            // 
            this.comboBoxAnimal.DataBindings.Add(new System.Windows.Forms.Binding("SelectedItem", this.bsManager, "SelectedAnimal", true));
            this.comboBoxAnimal.DataSource = this.bsAnimal;
            this.comboBoxAnimal.DisplayMember = "Name";
            this.comboBoxAnimal.Font = new System.Drawing.Font("Berlin Sans FB", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxAnimal.FormattingEnabled = true;
            this.comboBoxAnimal.Location = new System.Drawing.Point(440, 102);
            this.comboBoxAnimal.Name = "comboBoxAnimal";
            this.comboBoxAnimal.Size = new System.Drawing.Size(501, 31);
            this.comboBoxAnimal.TabIndex = 0;
            this.comboBoxAnimal.ValueMember = "AnimalId";
            // 
            // bsManager
            // 
            this.bsManager.DataSource = typeof(GTA.Business.Manager);
            // 
            // bsAnimal
            // 
            this.bsAnimal.DataMember = "Animals";
            this.bsAnimal.DataSource = typeof(GTA.Business.Manager);
            // 
            // tabAdmin
            // 
            this.tabAdmin.Controls.Add(this.label1);
            this.tabAdmin.Location = new System.Drawing.Point(4, 29);
            this.tabAdmin.Name = "tabAdmin";
            this.tabAdmin.Padding = new System.Windows.Forms.Padding(3);
            this.tabAdmin.Size = new System.Drawing.Size(1170, 486);
            this.tabAdmin.TabIndex = 1;
            this.tabAdmin.Text = "Administration";
            this.tabAdmin.UseVisualStyleBackColor = true;
            // 
            // buttonQuit
            // 
            this.buttonQuit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonQuit.Font = new System.Drawing.Font("Berlin Sans FB", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonQuit.Location = new System.Drawing.Point(1009, 423);
            this.buttonQuit.Name = "buttonQuit";
            this.buttonQuit.Size = new System.Drawing.Size(111, 38);
            this.buttonQuit.TabIndex = 4;
            this.buttonQuit.Text = "Quit";
            this.buttonQuit.UseVisualStyleBackColor = true;
            this.buttonQuit.Click += new System.EventHandler(this.ButtonQuitClicked);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Berlin Sans FB", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(303, 151);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(556, 35);
            this.label1.TabIndex = 0;
            this.label1.Text = "Time out, unable to complete this section";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1178, 519);
            this.ControlBox = false;
            this.Controls.Add(this.tabMainControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Guess the Animal";
            this.tabMainControl.ResumeLayout(false);
            this.tabPlay.ResumeLayout(false);
            this.tabPlay.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bsManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsAnimal)).EndInit();
            this.tabAdmin.ResumeLayout(false);
            this.tabAdmin.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabMainControl;
        private System.Windows.Forms.TabPage tabPlay;
        private System.Windows.Forms.TabPage tabAdmin;
        private System.Windows.Forms.Button buttonGo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxAnimal;
        private System.Windows.Forms.BindingSource bsAnimal;
        private System.Windows.Forms.BindingSource bsManager;
        private System.Windows.Forms.Button buttonQuit;
        private System.Windows.Forms.Label label1;
    }
}

