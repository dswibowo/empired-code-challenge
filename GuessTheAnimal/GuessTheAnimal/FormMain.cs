﻿using System;
using System.Linq;
using System.Windows.Forms;
using GTA.Business;
using GTA.Data;

namespace GuessTheAnimal
{
    public partial class FormMain : Form
    {
        /// <summary>
        /// Readonly member variable for the manager, which handles
        /// all business logic for the application.
        /// </summary>
        private readonly Manager _manager;

        public FormMain()
        {
            InitializeComponent();

            // Instantiate the manager for this application.
            _manager = new Manager();

            // Assign the manager as the datasource for UI bindings.
            bsAnimal.DataSource = _manager;
            bsManager.DataSource = _manager;
        }

        /// <summary>
        /// Handle the event when user click the Go button.
        /// Let's start the game!
        /// </summary>
        private void ButtonGoClick(object sender, EventArgs e)
        {
            const string caption = "Guessing...";

            // Get the manager ready.
            _manager.Reset();

            try
            {
                // Loop until we find an animal, or all options exhausted.
                while (_manager.PotentialAnimalIds.Count > 1)
                {
                    // Get the next question and ask the user.
                    Question nextQuestion = _manager.GetNextQuestions();

                    DialogResult result =
                        MessageBox.Show(this, nextQuestion.Question1,
                            caption,
                            MessageBoxButtons.YesNo);

                    if (result == DialogResult.Yes)
                    {
                        // Collect the correct question.
                        _manager.SubmitCorrectQuestion(nextQuestion.QuestionId);
                    }
                }

                if (_manager.PotentialAnimalIds.Count == 0)
                {
                    // All options exhausted with no result.
                    MessageBox.Show(this, "I give up, your answers don't make sense !!!");
                    return;
                }

                // There is 1 animal left, must be the answer.
                int animalId = _manager.PotentialAnimalIds.First();
                Animal answer = _manager.Animals.Single(a => a.AnimalId == animalId);

                MessageBox.Show(this, "The answer is " + answer.Name);
            }
            catch (Exception ex)
            {
                // Exceptions caught while asking the questions.
                string message = "Failed to find an answer, " + ex.Message;
                MessageBox.Show(this, message);
            }
        }

        /// <summary>
        /// Handle the event when the Quit button is clicked.
        /// </summary>
        private void ButtonQuitClicked(object sender, EventArgs e)
        {
            Close();
        }
    }
}
